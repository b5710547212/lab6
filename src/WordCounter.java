import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;
import java.io.IOException;
import java.net.URL;
import java.io.InputStream;

public class WordCounter {

	private Map<String,Integer> word = new HashMap();
	final static String DELIMS = "[\\s,.\\?!\"():;]+";
	
	public WordCounter()
	{
		
	}
	public void addWord(String word)
	{
		word = word.toLowerCase();
		if(this.word.containsKey(word))
		{
			this.word.replace(word, this.word.get(word), this.word.get(word)+1);
		}
		else
		{
			this.word.put(word,1);
		}
	}
	public Set<String> getWord()
	{
		return this.word.keySet();
	}
	public int getCounting(String word)
	{
		return this.word.get(word);
	}
	public String[] getSortedWords()
	{
		Set<String> input = this.getWord();
		List newInput = new ArrayList(input);
		Collections.sort(newInput);
		String[] temp = new String[newInput.size()];
		newInput.toArray(temp);
		/*for(int i=0 ; i<newInput.size() ;i++)
		{
			temp[i] = (String)newInput.get(i);
		}*/
		return temp;
	}
	public static void main(String[] args) throws IOException
	{
		String FILE_URL = "https://bitbucket.org/skeoop/oop/raw/5c13b8a3d871cdeaa3bf44a74cf0e3efa8099c82/week6/Alice-in-Wonderland.txt";
		URL url = new URL(FILE_URL);
		InputStream input = url.openStream();
		Scanner scanner = new Scanner(input);
		
		WordCounter count = new WordCounter();
		
		scanner.useDelimiter(DELIMS);
		
		while(scanner.hasNext())
		{
			count.addWord(scanner.next());
		}
		
		System.out.print(Arrays.toString(count.getSortedWords()));
		
	}
}
